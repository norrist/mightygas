#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#       

import string
import fuelups

'''
car_name, model, mpg, odometer, miles, gallons, price, city_percentage, 
fuelup_date, date_added, tags, notes, missed_fuelup, partial_fuelup, 
latitude, longitude
'''
#useful_headers=["odometer","gallons", "price","fuelup_date","missed_fuelup", "partial_fuelup"]
header_dict={}

def assign_headers(header_line): 
		#print header_line
		row_position=0
		#print useful_headers
		for item in header_line.split(', '): 
			#print str(item)
			#if str(item) in useful_headers: 
			#print item,row_position
			header_dict[item]=row_position 
			row_position+=1 
			
		#print header_dict
def header_position(header):
    return header_dict[header] 
    
    
    
def add_upload_data(csv_file,csv_source,current_user):  
	'''Process upladed uploaded csv data.

	Accepts csv data from main.py.  There is code to handle 2 csv 
	formats.  The mightygas format is simpler and a sample file is 
	provided on the upload page.  The Fuelly format is an export of 
	data from fuelly.com
	
	Once the csv file is parsed, the fuelup data is passed to fuelups.py
	'''
	count=0
	for row  in csv_file.split('\n'):
		#print row
		count+=1
		if count<2:
			assign_headers(row)
		else:
			if row=='':
				break
			#code for mightygas sample
			if csv_source=='mightygas':
				odometer,price,gallons,year,month,day=(
									row.split(',')[0],
									row.split(',')[1],
									row.split(',')[2],
									row.split(',')[3],
									row.split(',')[4],
									row.split(',')[5])
				if row.split(',')[6]:
					skip=True
				else:
					skip=False 
			#code for fuelly export
			if csv_source=='fuelly':
				csv_date=row.split(',')[header_position('fuelup_date')]
				date_no_quotes=csv_date.strip("'")
				
				odometer,price,gallons,=(
									row.split(',')[header_position('odometer')],
									row.split(',')[header_position('price')],
									row.split(',')[header_position('gallons')],
									)
				
				year,month,day=	string.split(string.split(date_no_quotes,' ')[0],'-')			
				if row.split(',')[header_position('partial_fuelup')]=='':
					skip=True
				elif row.split(',')[header_position('missed_fuelup')]=='':
					skip=True
				else:
					skip=False
			
			
			odometer=int(odometer.split('.')[0])
			price=float(price)
			gallons=float(gallons)
			year=int(year)
			month=int(month)
			day=int(day)
			skip=bool(skip)
			#send data to fuelups.py
			fuelups.add_fuelup(
				current_user,
				odometer,
				price,
				gallons,
				year,
				month, 
				day, 
				skip
							)
