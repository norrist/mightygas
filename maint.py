#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import datetime
import fuelups
import logging
from google.appengine.ext import db
from google.appengine.api import users

#type and kind are reserved py python, so types are now Ilk

class Ilk(db.Model):
	name = db.StringProperty()
	added = db.DateTimeProperty(auto_now_add=True)
	user = db.UserProperty()
	max_days = db.IntegerProperty()
	max_miles = db.IntegerProperty()
	last_date  = db.DateProperty()
	last_od  = db.IntegerProperty()
class Event(db.Model):
	ilk = db.StringProperty() 
	user = db.UserProperty()
	car = db.StringProperty() 
	date  = db.DateProperty()
	od = db.IntegerProperty()
	notes = db.TextProperty()
	cost = db.FloatProperty()

class Due(db.Model):
	ilk = db.StringProperty() 
	user = db.UserProperty()
	car = db.StringProperty() 
	last_date  = db.DateProperty()
	last_od  = db.IntegerProperty()
	due_date = db.DateProperty()
	due_days  = db.IntegerProperty()
	due_od  = db.IntegerProperty()
	overdue = db.BooleanProperty()
	performed = db.BooleanProperty()
	
def add_kind(name,user,max_days,max_miles):
	'''Adds a new type of maintencance for user'''
	ilk=Ilk()
	ilk.name=name
	ilk.user=user
	ilk.max_days=int(max_days)
	ilk.max_miles=int(max_miles)
	ilk.put()
	return True
def add_event(ilk,user,year,month,day,od,notes,cost):
	'''Adds new maintenance event'''
	event=Event()
	event.ilk=ilk
	event.car=fuelups.get_selected_car(user)
	event.user=user
	event.date=datetime.date(int(year),int(month),int(day))
	event.od=int(od)
	event.notes=notes
	event.cost=float(cost)
	event.put()
	return True
def list_kinds(user):
	'''Returns a list of maintenance types'''
	kind_list=[]
	ilk=Ilk.all().filter('user = ',user).order('max_miles')
	for line in ilk:
		kind_list.append(line.name)
	return kind_list

def calc_due(user,car):
	'''Calculates when maintenance events will be due'''
	
	##determine if there are enough fuelups to calc
	ok_to_calc=False
	stat_query=fuelups.RecentStats.all().filter('user = ',user)
	stat_query.filter('car = ',car)
	for car_stats in stat_query:
		if car_stats.mpd>0:
			logging.info("returned mpd: %s" %str(car_stats.mpd))
			ok_to_calc = True
		else:logging.info("returned mpd: %s" %str(car_stats.mpd))
	if not ok_to_calc:
		logging.error("not enough fuelup data ")
		return ##dont calc if not enough fuelup data
		
	#find last odometer reading
	fuelup=fuelups.Fuelup.all().order('od').filter('user = ', user)
	fuelup.filter('car = ',car)		
	for row in fuelup:
		last_fuelup_od= row.od	
		
	for ilk in list_kinds(user):
		logging.info('calculating due for %s' %ilk)
		event_query=Event.all().filter('user = ',user)
		event_query.filter('ilk = ',ilk).order('od')
		event_query.filter('car = ',car)
		performed_count=0		
		last_date=datetime.date(1900,1,1)
		last_od=0
		for event in event_query:
			last_date=event.date
			last_od=event.od
			performed_count+=1
		#print last_date,last_od,event.ilk
		due_update=Due.all().filter('user = ',user)
		due_update.filter('ilk = ',ilk)
		due_update.filter('car = ',car)
		for line in due_update:
			line.delete()
		new_due=Due()
		new_due.car=car
		new_due.ilk=ilk
		new_due.user=user
		new_due.last_date=last_date
		new_due.last_od=last_od
		ilk_max_miles_query=Ilk.all().filter('user = ',user)
		ilk_max_miles_query.filter('name = ',ilk)
		#print last_fuelup_od
		for next in ilk_max_miles_query:
			
			due_od_mpd=next.max_miles+new_due.last_od
			due_date_days=new_due.last_date+datetime.timedelta(days=next.max_days)
			new_due.due_date=last_date+datetime.timedelta(days=next.max_days)
			
		due_today_delta=new_due.due_date-datetime.date.today()
		new_due.due_od=due_od_mpd
		new_due.due_days=min(int((new_due.due_od-last_fuelup_od)/fuelups.mpd(user,car)),due_today_delta.days)
		new_due.overdue=new_due.due_days<0
		if performed_count>0:
			performed=True
		else:
			performed=False
		new_due.performed=performed
		new_due.put()

