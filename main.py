#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are
#  met:
#  
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following disclaimer
#    in the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of the  nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#	Some of the code (especially the template rendering and css) 
#	was take from the book:
#	Using Google App Engine by Charles Severance
#	Copyright May 2009 ISBN:978-0-596-80069-7

import os
import csv
import datetime
import string
import logging
import wsgiref.handlers
from google.appengine.api import users
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext import db
from google.appengine.api import users
##import project files
import fuelups,maint,import_rss,ErrorText,uploadFuelups,HelpText

def doRender(handler, template_name = 'index.html', values = { }):
	'''
	Render templates.  
	
	Expects: name of handler makeing call,
	name of template (default is index.html), and a dict (default is 
	empty dict)of values to pass to template.
	
	'''
	temp = os.path.join(
		os.path.dirname(__file__),
		'templates/' + template_name)
	if not os.path.isfile(temp):
		return False
	## Make a copy of the dictionary and add the path and session
	newval = dict(values)
	newval['path'] = handler.request.path
	##always pass user and date info to template
	newval['username'] = users.get_current_user()
	td=datetime.date.today()
	newval['today_month'] = td.month
	newval['today_day'] = td.day
	newval['today_year'] = td.year
	outstr = template.render(temp, newval)
	handler.response.out.write(outstr)
	return True



class AboutHandler(webapp.RequestHandler):
	'''Renders about page'''
	def get(self):
		doRender(self,'about.html')

class HelpHandler(webapp.RequestHandler):
	'''Renders a help page	'''
	def get(self):
		topic=self.request.get('topic')
		msg=HelpText.helptext(topic)
		doRender(self,'help.html',{'msg':msg})
		
class SkipHandler(webapp.RequestHandler):
	'''Skip on unskip a fuelup.
	
	The fuelup key and a redirect path (rdr) are passed here in the request
	URL'''
	def get(self):
		user = users.get_current_user()
		skip=self.request.get('key')
		fuelups.skip(skip,user)#send user and key to be processed 
		try:
			rdr=self.request.get('rdr')
		except:
			rdr='fuelups.html'
		self.redirect(rdr)

class DeleteHandler(webapp.RequestHandler):
	'''Delete a fuelup or maintenance event.
	
	The fuelup key and a redirect path (rdr) are passed here in the 
	request URL
	
	'''
	def get(self):
		user = users.get_current_user()
		car=self.request.cookies.get('selected_car')
		delete_key=self.request.get('key')
		fuelups.delete(delete_key,user)
		try:
			rdr=self.request.get('rdr')
		except:
			rdr='fuelups.html'
		if rdr=='maint.html':
				maint.calc_due(user,car)
		
		self.redirect(rdr)
class RecalcHandler(webapp.RequestHandler):
	'''Force maint due date recalculation.
	
	'''
	def get(self):
		user = users.get_current_user()
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		rdr='maint.html'
		maint.calc_due(user,selected_car)
		
		self.redirect(rdr)

class KindHandler(webapp.RequestHandler):
	'''Types of maintenance performed.
	
	Maintenance types (Oil change, etc) are refered to in the datastore
	as ILK.  The get requests passed the user's maintenance types to the
	template.  The post request is used to add new types nad recieves 
	data from the form in kinds.html
	
	'''
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))

		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		
		kind_query=maint.Ilk.all().filter('user = ', users.get_current_user()).order('max_miles')
		doRender(self,'kinds.html',{'kinds':kind_query})
	def post(self):
		try:
			maint.add_kind(
								
						self.request.get('name'),
						users.get_current_user(),
						self.request.get('max_days'),
						self.request.get('max_miles')
						)
		except:
			doRender(self,'error.html',{'msg':ErrorText.bad_data})
			return True
		self.redirect('/kinds.html')

class MaintHandler(webapp.RequestHandler):
	'''List and add mantenance events
	
		The get requests retrieves a list of maintenance events for the
		user/cat and sends it to the maint.html template.  The post 
		request handles the form to add new events.
	
	'''
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		#maint.calc_due(user,selected_car)
		kinds=maint.list_kinds(user)
		events=maint.Event.all().filter(' user = ',user).order('-od')
		events.filter('car = ',selected_car)
		due=maint.Due.all().filter(' user = ',user).order('due_days')
		due.filter('car = ',selected_car)
		doRender(self,'maint.html',{'car':selected_car,'kinds':kinds,'events':events, 'due':due})
		
	def post(self):
		selected_car=self.request.cookies.get('selected_car')
		user=users.get_current_user()
		try:
			datetime.date(int(self.request.get('year')),int(self.request.get('month')), int(self.request.get('day')))
		except:
			doRender(self,'error.html',{'msg':ErrorText.bad_date})
			return True
		try:
			if  self.request.get('cost')=='':
				cost=0.0
			else: cost=self.request.get('cost')
			maint.add_event(
									
									self.request.get('type'),
									users.get_current_user(),
									self.request.get('year'),
									self.request.get('month'), 
									self.request.get('day'),
									self.request.get('od'),
									self.request.get('notes'),
									cost,
									)
		except:
			doRender(self,'error.html',{'msg':ErrorText.bad_data})
			return True
		maint.calc_due(user,selected_car)
		self.redirect('/maint.html')

class UploadGasHandler(webapp.RequestHandler):
	'''Upload bulk fuelup data
	
	The get request loads the page with the upload form.  The post 
	request accepts the form data and passes the csv data and data type
	to uploadfuelups.py
	'''
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		fuelup_query=fuelups.Fuelup.all().order('-od').filter('user = ', users.get_current_user())
		fuelups.calcmpg(users.get_current_user(),selected_car)
		doRender(self,'upload_gas.html',{
											'fuelups':fuelup_query,
											'car':selected_car
											})
	def post(self):
		user = users.get_current_user()
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		uploadFuelups.add_upload_data(self.request.get('csv_file'),self.request.get('csv_source'),users.get_current_user())
		self.redirect('/fuelups.html')

class AllFuelupHandler(webapp.RequestHandler):
	'''Table with all fuelup data for user/car'''
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))

		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		fuelups.oldcalcmpg(users.get_current_user(),selected_car)
		fuelup_query=fuelups.Fuelup.all().order('-od').filter('user = ', users.get_current_user())
		fuelup_query.filter('car = ', selected_car)
		doRender(self,'allfuelups.html',{'fuelups':fuelup_query,
										'car':selected_car
										})
class FuelupHandler(webapp.RequestHandler):
	'''Display fuelup data
	
	The get request show the stats, mpg graph, table of last 20 fuelups
	and a form to add a fuelup.  The form is pre populated with todays
	date.  The post request handles the data from the form.
	'''
	def get(self):
		
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		
		##generate new  stats when the page loads
		fuelups.calcmpg(users.get_current_user(),selected_car)		
		all_fuelup_query=fuelups.Fuelup.all().order('-od').filter('user = ', users.get_current_user())
		all_fuelup_query.filter('car = ', selected_car)
		
		fuelup_query=all_fuelup_query.fetch(20)
		
		stats_query=fuelups.RecentStats.all().filter('user = ', users.get_current_user())
		stats_query.filter('car = ', selected_car)
		doRender(self,'fuelups.html',{'fuelups':fuelup_query,
										'stats':stats_query,
										'car':selected_car
										})
	def post(self):
		if self.request.get('skip'):
			skip=True
		else:skip=False
		try:
			datetime.date(int(self.request.get('year')),int(self.request.get('month')), int(self.request.get('day')))
		except:
			doRender(self,'error.html',{'msg':ErrorText.bad_date})
			return True
		fuelups.add_fuelup(
								users.get_current_user(),
								self.request.get('odometer'),
								self.request.get('price'),
								self.request.get('gallons'),
								self.request.get('year'),
								self.request.get('month'), 
								self.request.get('day'), 
								skip
								)
		self.redirect('/fuelups.html')
		
class CarsHandler(webapp.RequestHandler):
	'''Displays list of cars added by user and allows user to select car
	
	Selected car is the car that fuelups/maintnenace will be applied to
	'''
	def get(self):
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		car_query=fuelups.Car.all().filter('user = ', users.get_current_user())
		doRender(self,'cars.html',{'cars':car_query, 'car':selected_car})
	def post(self):
		if self.request.get('desc'):
			desc=self.request.get('desc')
		else:
			desc=''
		if self.request.get('name')=='':
			doRender(self,'error.html',{'msg':ErrorText.sellected_car_blank})
			return True
			
		fuelups.add_car(
							users.get_current_user(),
							self.request.get('name'),
							desc
							)
		self.response.headers.add_header('Set-Cookie', 'selected_car=%s' %str(self.request.get('name')))
		self.redirect('/cars.html')

class CarSelectHandler(webapp.RequestHandler):
	'''Selected car is stored in datastore and set in a cookie'''
	def get(self):
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))
			
		self.response.headers.add_header('Set-Cookie', 'selected_car=%s' %str(self.request.get('car')))
		fuelups.select_car(
							users.get_current_user(),
							self.request.get('car')
							)
		self.redirect('/cars.html')

class ImportMobileHandler(webapp.RequestHandler):
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))

		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		existing_fuelup_date=[]	
		fuelup_query=fuelups.Fuelup.all().order('-od').filter('user = ', users.get_current_user())
		fuelup_query.filter('car = ', selected_car)
		for line in fuelup_query:
			existing_fuelup_date.append(str(line.date))
				
		last_ten=fuelup_query.fetch(10)
		


		##read the mobile fuelups from the db
		mobile_query=fuelups.MobileImport.all().order('-date').filter('user = ', users.get_current_user())
		
		
		
		doRender(self,'mobile_import.html',{'fuelups':last_ten,
										'car':selected_car,
										'import':mobile_query,
										})


class RssHandler(webapp.RequestHandler):
	def get(self):
		selected_car=None
		user = users.get_current_user()
		if user:pass
		else:
			self.redirect(users.create_login_url(self.request.uri))

		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		existing_fuelup_date=[]	
		fuelup_query=fuelups.Fuelup.all().order('-od').filter('user = ', users.get_current_user())
		fuelup_query.filter('car = ', selected_car)
		for line in fuelup_query:
			existing_fuelup_date.append(str(line.date))
				
		last_ten=fuelup_query.fetch(10)
		
		feed_query=fuelups.Feed.all().filter('user = ', users.get_current_user())
		feed_query.filter('car = ', selected_car)
		##get the feed and pas the url to the import script
		feed_url=''#set to blank so page will display prior to adding feed
		for line in feed_query:
			feed_url=line.feed
		import_rss.fuelly(feed_url,user,selected_car,existing_fuelup_date)
		##read the rss fuelups from the db
		import_query=fuelups.RssImport.all().order('-date').filter('user = ', users.get_current_user())
		import_query.filter('car = ', selected_car)
		
		
		
		doRender(self,'rss_import.html',{'fuelups':last_ten,
										'car':selected_car,
										'import':import_query,
										'feed_url':feed_url
										})
	def post(self):
			fuelups.add_feed(
								users.get_current_user(),
								self.request.get('feed'),
								)
			self.redirect('/rss_import.html')

class ApplyRssHandler(webapp.RequestHandler):
	def get(self):
		user = users.get_current_user()
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		fuelup=self.request.get('key')
		try:
			rdr=self.request.get('rdr')
		except:
			rdr='rss_import.html'
		import_rss.apply_fuelup(fuelup,user,selected_car)
		self.redirect(rdr)
		
class ApplyMobileHandler(webapp.RequestHandler):
	def get(self):
		user = users.get_current_user()
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		mobile_fuelup=self.request.get('key')
		try:
			rdr=self.request.get('rdr')
		except:
			rdr='mobile_import.html'
			
		#import_mobile.apply_fuelup(fuelup,user,selected_car)
		mobile_add_query=fuelups.MobileImport()
		fuelup=fuelups.Fuelup()
		line=mobile_add_query.get(mobile_fuelup)
		fuelup=fuelups.Fuelup()
		fuelup.user=user
		fuelup.car=selected_car
		fuelup.od=line.od
		fuelup.price=line.price
		fuelup.gallons=line.gallons
		fuelup.date=line.date
		fuelup.skip=False
		fuelup.put()
		fuelups.calcmpg(user,selected_car)
		line.applied=True
		line.put()
		
		self.redirect(rdr)

class MobileHandler(webapp.RequestHandler):
	'''
	
	'''
	def get(self):
		doRender(self,'mobile.html',{})
	def post(self):
		logging.info(self.request.get('note'))

		if self.request.get('note'):
			note=self.request.get('note')
		else:note=''
		fuelups.add_mobile(
								self.request.get('mobile_user'),
								self.request.get('odometer'),
								self.request.get('price'),
								self.request.get('gallons'),
								note)
		self.redirect('/')

class MaintTestDataHandler(webapp.RequestHandler):
	'''
	Loads Maintenance data for testing.  Not usefull in production app.  
	'''
	def get(self):
		user = users.get_current_user()
		selected_car=self.request.cookies.get('selected_car')
		if selected_car==None:#try to get from datastore
			selected_car=fuelups.get_selected_car(user)
		if selected_car==None:#if not in datastore, error
			doRender(self,'error.html',{'msg':ErrorText.no_sellected_car})
			return True
		doRender(self,'maint_test_import.html',{'car':selected_car})
	def post(self):
		import maint_test_data
		for line in string.split(maint_test_data.test_data,'\n'):
			
			line_type = line.split(',')[0]
			if line_type=='Event':
				desc,month,day,year,od=line_type = line.split(',')[1:6]
				maint.add_event(desc,users.get_current_user(),year,month,day,od,'no note',0)
			elif line_type=='maint_type':
				desc,max_miles,max_days=line.split(',')[1:4]
				maint.add_kind(desc,users.get_current_user(),max_miles,max_days)
				#print line,'<br/>'
				
				
			#print  line
		self.redirect('/maint.html')

class LogoutHandler(webapp.RequestHandler):
	def get(self):		
		self.redirect(users.create_logout_url("/"))		
class LoginHandler(webapp.RequestHandler):
	def get(self):		
		self.redirect(users.create_login_url("/"))	
def main():
	application = webapp.WSGIApplication([
		('/about.html', AboutHandler),
		('/maint.html', MaintHandler),
		('/kinds.html', KindHandler),
		('/upload_gas.html', UploadGasHandler),
		('/cars.html', CarsHandler),
		('/select', CarSelectHandler),
		('/fuelups.html', FuelupHandler),
		('/help.html', HelpHandler),
		('/allfuelups.html', AllFuelupHandler),
		('/rss_import.html', RssHandler),
		('/mobile_import.html', ImportMobileHandler),
		('/maint_test_import.html', MaintTestDataHandler),
		('/m', MobileHandler),  
		('/skip', SkipHandler),  
		('/delete', DeleteHandler),
		('/recalc', RecalcHandler),
		('/apply_rss', ApplyRssHandler),
		('/apply_mobile', ApplyMobileHandler),
		('/login', LoginHandler),
		('/logout', LogoutHandler),
		('/.*', AboutHandler)],
		debug=True)
	wsgiref.handlers.CGIHandler().run(application)
  
if __name__ == '__main__':
	main()
