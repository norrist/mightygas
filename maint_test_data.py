'''
Contains coma seperated maintence events and types

First field defines event or type
if Event, fields are
	description,date,odometer,cost,notes
if type
	Description,days,miles

'''


test_data='''Event,Oil Change,9,25,2012,74061,55.03,
Event,Air Filter,6,27,2012,70580,0,
Event,Tire Replacement,6,3,2012,68783,709.82,
Event,Alignment,6,3,2012,68783,0,
Event,Tire Rotation,6,3,2012,68783,0,
Event,Oil Change,6,1,2012,68757,0,
Event,Alignment,3,19,2012,62991,206,Lifetime
Event,Oil Change,3,17,2012,62984,0,
Event,Tire Rotation,2,25,2012,61267,0,
Event,Thirty Thousand,2,10,2012,60278,0,
Event,Sixty Thousand,2,10,2012,60278,0,
Event,Oil Change,1,8,2012,57825,0,
Event,Air Filter,11,4,2011,52621,0,Inspection
Event,Oil Change,11,4,2011,52621,0,
Event,Tire Rotation,11,4,2011,52616,0,
Event,Oil Change,9,4,2011,47570,0,
Event,Tire Rotation,7,25,2011,44258,0,
Event,Oil Change,7,1,2011,42397,0,
Event,Oil Change,5,8,2011,37695,0,
Event,Air Filter,4,30,2011,37267,38,K&N
Event,Tire Rotation,4,11,2011,35888,0,
Event,Oil Change,2,27,2011,32600,None,
Event,Thirty Thousand,1,28,2011,30591,None,
Event,Air Filter,1,27,2011,30581,None,
Event,Oil Change,12,18,2010,27651,None,Synthetic
Event,Tire Rotation,1,14,2011,26893,None,
Event,Oil Change,11,7,2010,24268,None,
Event,Tire Rotation,9,24,2010,23112,None,
Event,Oil Change,9,17,2010,20718,None,
Event,Oil Change,8,7,2010,17188,None,
Event,Air Filter,7,5,2010,14973,None,
Event,Tire Rotation,7,5,2010,14713,None,
Event,Oil Change,6,24,2010,13903,None,
Event,Oil Change,5,9,2010,10608,None,
Event,Tire Rotation,4,9,2010,8513,None,
Event,Oil Change,3,27,2010,7213,None,
Event,Oil Change,6,10,2010,3880,None,
maint_type,Oil Change,90,5000,,
maint_type,Tire Rotation,180,7500,,
maint_type,Air Filter,365,15000,,
maint_type,Thirty Thousand,730,30000,,
maint_type,Alignment,730,30000,,
maint_type,Sixty Thousand,1460,60000,,
maint_type,Tire Replacement,1460,60000,,
'''
