#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import feedparser
import string
import datetime
import fuelups
from google.appengine.ext import db

months={
                    'January':1,
                    'February':2,
                    'March':3,
                    'April':4,
                    'May':5,
                    'June':6,
                    'July':7,
                    'August':8,
                    'September':9,
                    'October':10,
                    'November':11,
                    'December':12,
                    'Jan':1,
                    'Feb':2,
                    'Mar':3,
                    'Apr':4,
                    'May':5,
                    'Jun':6,
                    'Jul':7,
                    'Aug':8,
                    'Sep':9,
                    'Oct':10,
                    'Nov':11,
                    'Dec':12,
            }

def fuelly(feed_url,user,selected_car,existing_fuelup_date):

	rss=feedparser.parse(feed_url)

		##delete all the old fuelups in the import db
	rss_import_query=fuelups.RssImport().all().filter('user = ', user)
	rss_import_query.filter('car = ', selected_car)
	for line in rss_import_query:
		line.delete()


	for line in rss.entries:
		#month_string,day,year_string=string.split(line.title,' ')[12:-8]
		#day_string=day[:2]
		day_string,month_string,year_string=string.split(line.title,' ')[-5:-2]
		#print day_string,month_string,year_string
		trip_desc = string.split(line.description,":")[1]
		miles_float=float(string.split(trip_desc,"<")[0])
		gallon_desc = string.split(line.description,":")[2]
		gallons_float=float(string.split(gallon_desc,"<")[0])
		price_desc = string.split(line.description,":")[3][2:6]
		price_float=float(string.split(price_desc,"<")[0])
		fuelup_date=datetime.date(int(year_string),months[month_string],int(day_string))


		#add the new fuelups from feed to db
		fuelup=fuelups.RssImport()
		fuelup.user = user
		fuelup.car = selected_car
		fuelup.date  = fuelup_date
		fuelup.od = miles_float
		fuelup.gallons = gallons_float
		fuelup.price = price_float
		if str(fuelup.date) in existing_fuelup_date:
			fuelup.applied=True
		fuelup.put()

def apply_fuelup(rss_fuelup,user,selected_car):

	rss_add_query=fuelups.RssImport()
	fuelup=fuelups.Fuelup()
	line=rss_add_query.get(rss_fuelup)
	fuelup=fuelups.Fuelup()
	fuelup.user=user
	fuelup.car=selected_car
	fuelup.od=line.od+last_od(user,selected_car)
	fuelup.price=line.price
	fuelup.gallons=line.gallons
	fuelup.date=line.date
	fuelup.skip=False
	fuelup.put()
	fuelups.calcmpg(user,selected_car)

def last_od(user,selected_car):
	stat_query=fuelups.RecentStats.all().filter('user = ',user)
	stat_query.filter('car = ',selected_car)
	for car_stats in stat_query:
		if car_stats.last_od==None:
			return 0
		else:
			return car_stats.last_od
