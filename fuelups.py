#       Redistribution and use in source and binary forms, with or without
#       modification, are permitted provided that the following conditions are
#       met:
#       
#       * Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       * Redistributions in binary form must reproduce the above
#         copyright notice, this list of conditions and the following disclaimer
#         in the documentation and/or other materials provided with the
#         distribution.
#       * Neither the name of the  nor the names of its
#         contributors may be used to endorse or promote products derived from
#         this software without specific prior written permission.
#       
#       THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#       "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#       LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#       A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#       OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#       SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#       LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#       DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#       THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#       (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#       OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import datetime
import logging
from google.appengine.ext import db
from google.appengine.api import users

class Fuelup(db.Model):
	user = db.UserProperty()
	car= db.StringProperty()
	date  = db.DateProperty()
	added = db.DateTimeProperty(auto_now_add=True)
	od = db.FloatProperty()
	gallons = db.FloatProperty()
	price = db.FloatProperty()
	mpg = db.FloatProperty()
	skip = db.BooleanProperty()
	trip=db.FloatProperty()
	days=db.IntegerProperty()
class RssImport(db.Model):#data from rss feed
	user = db.UserProperty()
	car= db.StringProperty()
	date  = db.DateProperty()
	added = db.DateTimeProperty(auto_now_add=True)
	od = db.FloatProperty()
	gallons = db.FloatProperty()
	price = db.FloatProperty()
	applied = db.BooleanProperty()
	trip=db.FloatProperty()
class MobileImport(db.Model):#data from rss feed
	user = db.UserProperty()
	date = db.DateProperty(auto_now_add=True)
	od = db.FloatProperty()
	gallons = db.FloatProperty()
	price = db.FloatProperty()
	applied = db.BooleanProperty()
	note = db.StringProperty()


class Feed(db.Model):#fuelly rss feed URL
	user = db.UserProperty()
	car = db.StringProperty()
	feed = db.StringProperty()
class Car(db.Model):
	name = db.StringProperty()
	description = db.TextProperty()
	user = db.UserProperty()
	selected = db.BooleanProperty()
	#maint_calc_ok = db.BooleanProperty()
class Stats(db.Model):
	car = db.StringProperty()
	user = db.UserProperty()
	total_price=db.FloatProperty()
	total_trip=db.FloatProperty()
	total_count=db.IntegerProperty()
	avg_mpg=db.StringProperty()
	avg_trip=db.StringProperty()
	total_mpg=db.FloatProperty()
	skip_count=db.IntegerProperty()
	best_mpg=db.StringProperty()
	best_price=db.FloatProperty()
	url=db.TextProperty()
	day_rate=db.StringProperty()#mpd as formatted string
	mpd=db.FloatProperty()
	last_od = db.FloatProperty()
class RecentStats(db.Model):
	car = db.StringProperty()
	user = db.UserProperty()
	avg_mpg=db.StringProperty()
	avg_trip=db.StringProperty()
	url=db.TextProperty()
	day_rate=db.StringProperty()
	mpd=db.FloatProperty()
	last_od = db.FloatProperty()
def add_fuelup(user,od,price,gallons,year,month,day,skip):
	'''Add fuelup data to datasore'''
	fuelup=Fuelup()
	fuelup.car=get_selected_car(user)
	fuelup.user=user
	#fuelup.car=car
	fuelup.od=float(od)
	fuelup.price=float(price)
	fuelup.gallons=float(gallons)
	fuelup.date=datetime.date(int(year),int(month),int(day))
	if skip:
		fuelup.skip=True
	else:
		fuelup.skip=False
	fuelup.put()
	#car_maint_calk_ok=Car().all().filter('user = ', user)
	#car_maint_calk_ok.filter('name = ',fuelup.car)
	#for car_to_update in car_maint_calk_ok:
	#	car_to_update.maint_calc_ok=True
	#	logging.error("Turning car OK to calc True")
	#	car_to_update.put()

def add_mobile(user,od,price,gallons,note):
	'''Add fuelup data to datasore'''
	logging.info("adding_mobile")
	fuelup=MobileImport()
	fuelup.user = users.User(user+"@gmail.com")
	fuelup.od=float(od)
	fuelup.price=float(price)
	fuelup.gallons=float(gallons)
	fuelup.note=note
	fuelup.put()

def add_feed(user,feed):
	'''Add a Fuelly.com RSS feed URL
	
	The feed will be parsed for fuelup data and may be added as felup 
	data by the user.
	'''
	import_feed=Feed()
	import_feed.car=get_selected_car(user)
	import_feed.user=user
	import_feed.feed=feed
	import_feed.put()
	return True
	
def add_car(user,car_name,desc):
	'''Add a new vehicle
	
	Description is optional.  Makes a newly added cat the selectec car.
	'''
	car=Car()
	car.user=user
	car.name=car_name
	car.description=desc
	car.put()
	select_car(user,car_name)
	return True

def select_car(user,select_car_name):
	'''Only one car per user can have select'''
	cars=Car().all().filter('user = ', user)
	for car in cars:
		if car.name==select_car_name:
			car.selected=True
			car.put()
			
		else:
			car.selected=None
			car.put()
	 
	return True

def get_selected_car(user):
	'''Returns selected car for a given user
	
	Returns None if not in datastore
	'''
	selected_name=None#set to None if not in datastore
	cars=Car().all().filter("selected = ", True)
	cars.filter('user = ', user)
	for car in cars:
		selected_name=car.name
	return selected_name
	
def delete(key,user):
	'''Deletes given key'''
	#Need to add logic to verify key belongs to user
	db.delete(db.Key(key))
	
def skip(key,user):
	'''Skip/unskip a fuelup.
	
	Skipped fuelups are not used in any of the calculations.
	Function expects the fuelup key and the user that is making the
	request.  The function changes the booleanstate of the value 'skip
	in the Fuelup table'''
	skip_fuelup=Fuelup.all().filter('__key__ = ',(db.Key(key)))
	skip_fuelup.filter('user = ',user)#skip request must be by user that added it 
	for line in skip_fuelup:
		if line.skip==True:
			line.skip=False
		else:
			line.skip=True
		line.put()

def oldcalcmpg(user,car):
	'''calculate mpg and other statistics for each fuelup. At the end,
	it calls the function to calculate other statistics for the car 
	passing the data from the query.  '''
	fuelup=Fuelup.all().order('od').filter('user = ', user)
	fuelup.filter('car = ',car)
	last_od=0
	last_date=datetime.date.today() #set variable
	for line in fuelup:
		if line.trip!=line.od-last_od: #only calc if previous fuelup changed
			if last_od==0:
				mpg=0
				trip=0
				days=0
			elif line.skip:
				mpg=0
				trip=0
				days=0
			else:
				trip=line.od-last_od			
				mpg=trip/line.gallons
				trip_days=(line.date-last_day)
				days=trip_days.days
			line.mpg=float(mpg)
			line.trip=float(trip)
			line.days=days
			line.put()
		last_od=line.od
		last_day=line.date
	stats(fuelup,user,car)
	
def calcmpg(user,car):
	'''calculate mpg and other statistics for each fuelup. 
	
	Only calculates the data for last 20 fuelups
	At the end,	it calls the function to calculate other statistics for 
	the car passing the data from the query.  '''
	fuelup_query=Fuelup.all().order('-od').filter('user = ', user)
	fuelup_query.filter('car = ',car)
	fuelup = fuelup_query.fetch(21)
	fuelup.reverse()
	last_od=0
	last_date=datetime.date.today() #set variable
	for line in fuelup:
		if line.trip!=line.od-last_od: #only calc if previous fuelup changed
			if last_od==0:
				mpg=0
				trip=0
				days=0
			elif line.skip:
				mpg=0
				trip=0
				days=0
			else:
				trip=line.od-last_od			
				mpg=trip/line.gallons
				trip_days=(line.date-last_day)
				days=trip_days.days
			line.mpg=float(mpg)
			line.trip=float(trip)
			line.days=days
			line.put()
		last_od=line.od
		last_day=line.date
	recentstats(fuelup,user,car)
		
def stats(fuelup,user,car):
	'''Calculats stats for user/car - called by oldcalcmpg()'''
	mpg_list={}
	cars_last_od=0.0
	try:
		low_mpg=count_for_avg=best_price=best_mpg=skip_count=total_mpg=total_price=total_trip=total_count=0
		low_mpg=100
		for line in fuelup:
			#the first time the items are calced after a csv upload
			#some fields will not yet be set, so set to zero
			if line.mpg==None:
				line.mpg=0.0
				line.trip=0.0
			if line.skip:
				line.mpg=0.0
				line.trip=0.0
			if not line.skip:
				skip_count=skip_count+1
				total_mpg=total_mpg+line.mpg
				mpg_list[count_for_avg]=line.mpg
			if line.mpg<>0:
				count_for_avg+=1
				if line.mpg<low_mpg:
					low_mpg=line.mpg
			#if cars_last_od==0:
			cars_last_od=line.od
			total_trip=total_trip+line.trip
			total_count=total_count+1
			total_price=total_price+line.price
			if (not line.skip) and line.mpg>best_mpg:
				best_mpg=line.mpg
			if (best_price==0) or (best_price>line.price):
				best_price=line.price
		if count_for_avg>0:
			avg_mpg=total_mpg/count_for_avg
		else:
			avg_mpg=0
		if total_count>0:
			avg_trip=total_trip/total_count
		else:
			avg_trip=0
		url=chart_url(mpg_list,best_mpg,low_mpg)
		day_rate=mpd(user,car,fuelup)
		
		
		if day_rate==None:
			day_rate=0.0
		#delete the old stats 
		stat_update=Stats.all().filter('user = ',user)
		stat_update.filter('car = ',car)
		for line in stat_update:
			line.delete()
		#insert the new stats
		stats=Stats()
		stats.user=user
		stats.car=car
		stats.total_price=float(total_price)
		stats.total_trip=float(total_trip)
		stats.total_count=total_count
		stats.avg_mpg='%.2f' %avg_mpg
		stats.avg_trip='%.2f' %avg_trip
		stats.total_mpg=float(total_mpg)
		stats.skip_count=skip_count
		stats.best_mpg='%.2f' %best_mpg
		stats.best_price=float(best_price)
		stats.url=url
		stats.day_rate='%.2f' %day_rate
		stats.mpd=float(day_rate)
		stats.last_od=cars_last_od
		stats.put()
	except IndexError: #ZeroDivisionError:
			pass	
def recentstats(fuelup,user,car):
	'''Calculats stats for user/car - called by calcmpg()
	
	Only passed 20 Fuelups and calculates some averages
	'''
	mpg_list={}
	cars_last_od=0.0
	try:
		best_mpg=count_for_avg=skip_count=total_mpg=total_trip=total_count=0
		low_mpg=100
		for line in fuelup:
			#the first time the items are calced after a csv upload
			#some fields will not yet be set, so set to zero
			if line.mpg==None:
				line.mpg=0.0
				line.trip=0.0
			if (not line.skip) and line.mpg>best_mpg:
				best_mpg=line.mpg
			if not line.skip:
				skip_count=skip_count+1
				total_mpg=total_mpg+line.mpg
				mpg_list[count_for_avg]=line.mpg
			if line.mpg<>0:
				count_for_avg+=1
				if line.mpg<low_mpg:
					low_mpg=line.mpg
			#if cars_last_od==0:
			cars_last_od=line.od
			total_trip=total_trip+line.trip
			total_count=total_count+1
			
		if count_for_avg>0:
			avg_mpg=total_mpg/count_for_avg
		else:
			avg_mpg=0
		if total_count>0:
			avg_trip=total_trip/total_count
		else:
			avg_trip=0
		url=chart_url(mpg_list,best_mpg,low_mpg)
		day_rate=mpd(user,car,fuelup)
		
		
		if day_rate==None:
			day_rate=0.0
		#delete the old stats 
		stat_update=RecentStats.all().filter('user = ',user)
		stat_update.filter('car = ',car)
		for line in stat_update:
			line.delete()
		#insert the new stats
		stats=RecentStats()
		stats.user=user
		stats.car=car
		stats.avg_mpg='%.2f' %avg_mpg
		stats.avg_trip='%.2f' %avg_trip
		stats.total_mpg=float(total_mpg)
		stats.url=url
		stats.day_rate='%.2f' %day_rate
		stats.mpd=float(day_rate)
		stats.last_od=cars_last_od
		stats.put()
		
	except IndexError: #ZeroDivisionError:
			pass
def chart_url(mpg_list,best_mpg,low_mpg,limit=21):
	'''Generates URL for mpg chart
	
	Called by stats().  Passed a list of mpg for each fuelup and upper
	and lower limits  Builds URL for google's chart API.  
	'''
	logging.info(mpg_list)
	try:
		url_start='http://chart.apis.google.com/chart?cht=lc&chf=bg,s,FFFFFF&chco=447788&chd=t:'
		url_data=''
		mpg_from_dict=[] #put all the MPG into a list to find a min and max for the chart
		#for i in range(len(mpg_list)):
		for i in (mpg_list):
			#if len(mpg_list)-limit<=i:
				try:
					url_data+=str('%.02f') %mpg_list[i]
					url_data+=','
					mpg_from_dict.append(mpg_list[i])
				except:pass
		range_high=int(max(mpg_from_dict[-limit:]))
		range_low=int(min(mpg_from_dict[-limit:]))	
		range_str='&chxr=0,%.2f,%.2f,1&chds=%.2f,%.2f' %(range_low,range_high,range_low,range_high)
		url_end='&chs=250x100&chxt=y'
		url=url_start+url_data[:-1]+range_str+url_end
		#if there is only one fuelup, the graph doesn't work, so return the static image
		if len(mpg_list)<2:
			url='/static/no_graph.png'
		return url
	except :
		return '/static/no_graph.png'
def mpd(user,car,fuelup=None):
	'''Calculates miles per day
	
	passed fuelup date when called by calc().  Querys datestore for 
	fuelup data when called by maint.py
	'''
	try:
		fuelup_count=0
		total_trip=0
		total_days=0
		if fuelup==None:
			fuelup=Fuelup.all().order('-od').filter('user = ', user)
			fuelup.filter('car = ',car)
		
		for line in fuelup:
			fuelup_count+=1
			if fuelup_count>10:
				continue
			if fuelup_count==1:
				late_date=line.date
			early_date=line.date
			total_trip+=line.trip
			total_days+=line.days
		return total_trip/total_days
	except :#ZeroDivisionError:
		return
	
		
	
