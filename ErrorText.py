##file contains error messages

no_sellected_car='''

Before you can add any data to the site, 
you have to add a car.  
<br />
Click on Cars in the menu to add a car
'''

bad_date='''
The site doesn't understand the date you entered.
<br />
Please make sure you entered a 4 digit year
and 2 digits for the month and year
'''

bad_data='''
There is a problem with the data you entered.
<br />
Some of the common problems are leaving a required field blank 
or have letters where numbers are expected.
'''
sellected_car_blank='''
The can description can be blank, but there has to be a car name
'''
