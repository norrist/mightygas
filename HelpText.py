##file contains help messages
## Not used

maintenance_text='''

Before you can add a maintenance event, you have to add one or more 
maintenance types.  The name of each type of maintenance will be in 
the drop down menu of the form.   
<p>
To record maintenance on your vehicle, select the type of maintenance 
from the drop down and fill in the date and odometer reading.  If you 
want, you can also record the price and any notes.  
<p>
There are two tables at the top of the maintenance page.  The first 
lists information concerning when each type of maintenance will be due.  


<ul>
	<li>The odometer column is the odometer reading plus the maximum 
	distance 	entered for the maintenance type.</li>
	<li>The date column is the date the maintenance was last performed 
	plus the maximum numbers of days entered 	for the maintenance 
	type.</li>
	<li>The Due in * days column guesses the number of days before you 
	will reach either the due odometer reading or due 	date.</li>
</ul>

    
<p>
The other table is a list of all the record maintenance events.  
If you entered some information incorrectly, you can click the x in 
the table, and delete that event.  
'''

def helptext(topic):
	if topic=='maintenance':
		return maintenance_text
